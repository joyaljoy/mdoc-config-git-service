package mdoc.one.service;

import java.io.File;

import org.eclipse.jgit.api.CloneCommand;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullCommand;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.PushCommand;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.transport.PushResult;
import org.eclipse.jgit.transport.SshSessionFactory;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.transport.OpenSshConfig.Host;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.jcraft.jsch.Session;
import com.jcraft.jsch.UserInfo;

@Service
public class GitService {
	

	private String mdocConfigGitUrl = "git://bitbucket.org/mdocplatform/mdoc-configuration.git";
//	private String mdocConfigGitUrl = "https://joyaljoy@bitbucket.org/mdocplatform/mdoc-configuration.git";
//	private String mdocConfigGitUrl = "https://bitbucket.org/mdocplatform/mdoc-configuration";
	private static String mdocConfigLocalDirectory = "/Users/joyaljoy/Documents/mDoc/Projects/Backup Files/jgit/mdoc-configuration";

	private final CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider("mdoc_pranav",
			"YJBWApcskkg6duQNkHwN");

	public void cloneRepository() throws Exception {

		SshSessionFactory sshSessionFactory = new JschConfigSessionFactory() {
			@Override
			protected void configure(Host host, Session session) {
				session.setPassword("Jesus@1994");
			}

//			@Override
//			protected JSch createDefaultJSch(FS fs) throws JSchException {
//			    JSch defaultJSch = super.createDefaultJSch(fs);
//			    defaultJSch.removeAllIdentity();
//			    defaultJSch.addIdentity("/Users/joyaljoy/Documents/mDoc/Projects/mdoc-config-git-service/src/main/resources/git-ssh-keys/id_rsa", "Jesus@1994");
//			    return defaultJSch;
//			}
		};

//		Git.cloneRepository().setURI(mdocConfigGitUrl)
////				.setBranchesToClone(Arrays.asList("refs/heads/specific-branch"))
////				.setBranch("refs/heads/specific-branch")
////				.setCredentialsProvider(credentialsProvider)
//				.setTransportConfigCallback(transport -> {
//					SshTransport sshTransport = (SshTransport) transport;
//					sshTransport.setSshSessionFactory(getSessionFactory());
//				}).setDirectory(new File(mdocConfigLocalDirectory))
//				.call();

		CloneCommand cloneCommand = Git.cloneRepository();
//		cloneCommand.setTransportConfigCallback( new TransportConfigCallback() {
//		  @Override
//		  public void configure( Transport transport ) {
//		    if( transport instanceof SshTransport ) {
//		      SshTransport sshTransport = ( SshTransport )transport;
//		      sshTransport.setSshSessionFactory( sshSessionFactory);
//		    }
//		  }
//		} );
		cloneCommand.setURI(mdocConfigGitUrl).setDirectory(new File(mdocConfigLocalDirectory));
		cloneCommand.setCredentialsProvider(credentialsProvider);
		cloneCommand.call();
	}

	public void takePull() throws Exception {
		Git git = Git.open(new File(mdocConfigLocalDirectory));
		PullCommand pullCommand = git.pull();
		pullCommand.setCredentialsProvider(credentialsProvider);
		PullResult gitPullResponse = pullCommand.call();
		System.out.println(gitPullResponse.toString());
	}

	public void commitAndPushChanges() throws Exception {
		Git git = Git.open(new File(mdocConfigLocalDirectory));

//		// add remote repo:
//	    RemoteAddCommand remoteAddCommand = git.remoteAdd();
//	    remoteAddCommand.setName("origin");
//	    remoteAddCommand.setUri(new URIish(mdocConfigGitUrl));
//	    // you can add more settings here if needed
//	    remoteAddCommand.call();

//		git.remoteList().call().forEach(remote -> System.out.print("Git remote: "+remote.toString() + ", "));
//		System.out.println();
//		git.branchList().call().forEach(branch -> System.out.print("Git branch: "+branch.toString() + ", "));
//		System.out.println("git status >> " + git.status().call().toString());
//		
//		AddCommand addCommand = git.add();
//		addCommand.addFilepattern(".");
//		DirCache result = addCommand.call();
//		System.out.println("git add >> " + result.toString());
//		
//	    CommitCommand gitCommitCommand = git.commit();
//	    gitCommitCommand.setMessage("Changes");
//	    RevCommit commit = gitCommitCommand.call();
//	    System.out.println("git commit >> " + commit.toString());

		// push to remote:
		PushCommand pushCommand = git.push();
		pushCommand.setCredentialsProvider(credentialsProvider);
		Iterable<PushResult> gitPushResult = pushCommand.call();
		gitPushResult.forEach(pushResult -> System.out.println("git push >> " + pushResult.toString()));
	}

	private SshSessionFactory getSessionFactory() {
		return new JschConfigSessionFactory() {
			@Override
			protected void configure(OpenSshConfig.Host host, Session session) {
				session.setUserInfo(new UserInfo() {
					@Override
					public String getPassphrase() {
						return "Jesus@1994";
					}

					@Override
					public String getPassword() {
						return null;
					}

					@Override
					public boolean promptPassword(String message) {
						return false;
					}

					@Override
					public boolean promptPassphrase(String message) {
						return true;
					}

					@Override
					public boolean promptYesNo(String message) {
						return false;
					}

					@Override
					public void showMessage(String message) {
					}
				});
			}

		};
	}

}
