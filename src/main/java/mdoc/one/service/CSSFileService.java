package mdoc.one.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import mdoc.one.model.Configs;
import mdoc.one.model.Property;
import mdoc.one.model.Property.PropertyBuilder;
import mdoc.one.model.PropertyType;

@Service
public class CSSFileService {

	private static String mdocConfigLocalDirectory = "/Users/joyaljoy/Documents/mDoc/Projects/Backup Files/jgit/mdoc-configuration/";

	public List<Configs> getCSSProperties(String application, String profile, String label) throws IOException {

		List<File> configFilesList = listfiles(mdocConfigLocalDirectory + application + "/" + profile,
				new ArrayList<>(), true);
		List<Configs> propertiesList = new ArrayList<>();

		for (File configFile : configFilesList) {
			String filePath = configFile.getPath();
			if (configFile.getName().endsWith(".css")) {
				Configs configs = Configs.builder().propertySource(filePath.split(application + "/" + profile + "/")[1])
						.properties(readFileLineByLineIntoPropertyMap(filePath)).application(application)
						.profile(profile).build();
				propertiesList.add(configs);
			}
		}

		return propertiesList;
	}

	public void updateConfigs(List<Configs> configUpdates) {
		for (Configs configUpdate : configUpdates) {
			updateProperty(configUpdate);
		}
	}

	private List<String> getDirectoriesInTheDirectory(String mdocConfigLocalDirectory) {
		// Creating a File object for directory
		File directoryPath = new File(mdocConfigLocalDirectory);

		// List of all directories
		String contents[] = directoryPath.list(new FilenameFilter() {
			@Override
			public boolean accept(File current, String name) {
				File file = new File(current, name);
				if (file.isDirectory()) {
					if (file.getName().startsWith("."))
						return false;
					return true;
				}
				return false;
			}
		});
		return Arrays.asList(contents);
	}

	private List<File> listfiles(String directoryName, List<File> files, boolean configFileList) {
		File directory = new File(directoryName);

		// Get all files from a directory.
		File[] fList = directory.listFiles();
		if (fList != null) {
			for (File file : fList) {
				if (file.isFile()) {
					if (configFileList == true && (file.getName().endsWith(".properties")
							|| file.getName().endsWith(".json") || file.getName().endsWith(".css")))
						files.add(file);
					else if (configFileList == false)
						files.add(file);
				} else if (file.isDirectory()) {
					listfiles(file.getPath(), files, configFileList);
				}
			}
		}
		return files;
	}

	private List<Property> readFileLineByLineIntoPropertyMap(String filePath) {

		List<Property> singleFilePropertyList = new ArrayList<>();
		try {
			File file = new File(filePath);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while ((line = br.readLine()) != null) {
				line = line.trim();
				if (line != null && !line.isEmpty()) {
					String[] propertyValue;
					if (!line.contains(":root") && !line.contains("}") && !line.startsWith("/*") && !line.endsWith("*/")) {
						propertyValue = line.split(":");
						if (propertyValue.length == 2) {
							propertyValue[0] = propertyValue[0].trim();
							propertyValue[1] = propertyValue[1].trim();
							if (propertyValue[0].startsWith("--")) {
								propertyValue[0] = (String) propertyValue[0].substring(2, propertyValue[0].length());
							}
							if (propertyValue[1].endsWith(";")) {
								propertyValue[1] = (String) propertyValue[1].substring(0,
										propertyValue[1].length() - 1);
							}
							singleFilePropertyList.add(getProperty(filePath, propertyValue[0], propertyValue[1]));
							System.out.println("property: " + propertyValue[0] + ", value: " + propertyValue[1]);
						}
					}
				}
			}
			fr.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return singleFilePropertyList;
	}

	private Property getProperty(String filePath, String propertyName, String propertyValue) {

		PropertyBuilder property = Property.builder().propertyName(propertyName).currentValue(propertyValue).newValue(null);
		if (propertyValue.startsWith("#") || propertyValue.startsWith("rgba(")) {
			property.propertyType(PropertyType.COLOR_PICKER);
		} else if (propertyValue.startsWith("url(")) {
			// Set fileName only here
//			property.currentValue(propertyValue);
			property.propertyType(PropertyType.FILE_UPLOAD);
		} else {
			property.propertyType(PropertyType.TEXT);
		}

		return property.build();
	}

	public void updateProperty(Configs configUpdate) {
		try {
			String filePath = mdocConfigLocalDirectory + configUpdate.getApplication() + "/" + configUpdate.getProfile()
					+ "/" + configUpdate.getPropertySource();
			BufferedReader file = new BufferedReader(new FileReader(filePath));
			StringBuffer inputBuffer = new StringBuffer();
			String line;

			while ((line = file.readLine()) != null) {

				final String propertyKey = line.trim().split("=")[0];
				String updatedLine = configUpdate.getProperties().stream()
						.filter(property -> property.getPropertyName().equalsIgnoreCase(propertyKey))
						.map(property -> property.getPropertyName() + "=" + property.getNewValue()).findFirst()
						.orElse(null);
				if (updatedLine != null)
					line = updatedLine;
				inputBuffer.append(line);
				inputBuffer.append('\n');
			}
			file.close();

//			System.out.println(inputBuffer.toString());

			// write the new string with the replaced line OVER the same file
			FileOutputStream fileOut = new FileOutputStream(filePath);
			fileOut.write(inputBuffer.toString().trim().getBytes());
			fileOut.close();

		} catch (Exception e) {
			System.out.println("Problem reading file.");
		}
	}

}
