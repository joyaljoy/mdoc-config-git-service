package mdoc.one.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mdoc.one.model.Configs;
import mdoc.one.service.CSSFileService;
import mdoc.one.service.PropertiesFileService;

@RestController
@RequestMapping("/config")
@CrossOrigin(origins = "http://localhost:4200") // delete this
public class ConfigController {

	@Autowired
	PropertiesFileService propertiesFileService;
	
	@Autowired
	CSSFileService cssFileService;

	@GetMapping("/{application}/{profile}/{label}")
	public List<Configs> cloneRepository(@PathVariable("application") String application,
			@PathVariable("profile") String profile, @PathVariable("label") String label) throws Exception {
//		return propertiesFileService.getProperties(application, profile, label);
		return cssFileService.getCSSProperties(application, profile, label);
	}
	
	@PostMapping(path = "/configupdate", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> webHookReciever(@RequestBody List<Configs> configUpdates) {
		propertiesFileService.updateConfigs(configUpdates);
		return new ResponseEntity<>("Success", HttpStatus.OK);
	}
}
