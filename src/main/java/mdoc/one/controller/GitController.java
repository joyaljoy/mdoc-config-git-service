package mdoc.one.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mdoc.one.service.GitService;

@RestController
@RequestMapping("/git")
public class GitController {

	@Autowired
	GitService gitService;

	@GetMapping("/clone")
	public void cloneRepository() throws Exception {
		gitService.cloneRepository();
	}

	@PutMapping("/pull")
	public void takePull() throws Exception {
		gitService.takePull();
	}

	@PostMapping("/commitAndPush")
	public void commitAndPushChanges() throws Exception {
		gitService.commitAndPushChanges();
	}

}
