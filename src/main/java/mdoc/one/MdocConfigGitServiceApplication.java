package mdoc.one;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MdocConfigGitServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MdocConfigGitServiceApplication.class, args);
	}

}
