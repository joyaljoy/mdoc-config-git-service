package mdoc.one.model;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Configs {

	String propertySource;
	String application;
	String profile;
	List<Property> properties;
}
