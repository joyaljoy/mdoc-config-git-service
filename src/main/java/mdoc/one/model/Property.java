package mdoc.one.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Property {

//	String propertySource;
//	String app;
//	String profile;
	PropertyType propertyType;
	String propertyName;
	Object currentValue;
	Object newValue;
}
