package mdoc.one.model;

public enum PropertyType {
//	SPRING, CSS, MOBILE, IMAGE
	TOGGLE_BUTTON("togglebutton"), 
	TEXT("text"),
	COLOR_PICKER("colorpicker"),
	FILE_UPLOAD("fileupload");
	
	private final String propertyType;
	
	PropertyType(String propertyType) {
		this.propertyType = propertyType;
	}
}
